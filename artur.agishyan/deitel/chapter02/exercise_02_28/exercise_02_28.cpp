#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter a 5 digit number: ";
    std::cin >> number;

    if (number / 100000 != 0) {
       std::cout << "Error 1: not a 5 digit number\n";
       return 1;
    }
    std::cout << (number / 10000 % 10) << "   "
              << (number / 1000 % 10) << "   "
              << (number / 100 % 10) << "   "
              << (number / 10 % 10) << "   "
              << (number % 10) << std::endl;
    return 0;
}

