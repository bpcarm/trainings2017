#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter four-digit number: ";
    std::cin >> number;

    if (number < 1000) {
        std::cerr << "Error 1. Entered number isn't four-digit." << std::endl;
        return 1;
    }
    if (number > 9999) {
        std::cerr << "Error 1. Entered number isn't four-digit." << std::endl;
        return 1;
    }

    int newDigit1 = (number / 10 + 7) % 10;
    int newDigit2 = (number + 7) % 10;
    int newDigit3 = (number / 1000 + 7) % 10;
    int newDigit4 = (number / 100 + 7) % 10;

    std::cout << "Encrypted number: " << newDigit1 << newDigit2 
        << newDigit3 << newDigit4 << std::endl;

    return 0;
}

