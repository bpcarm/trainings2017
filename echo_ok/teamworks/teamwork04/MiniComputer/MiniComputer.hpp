#include <string>

class MiniComputer
{
public:
    MiniComputer(std::string name);
    int run();
    std::string getComputerName();
    void setComputerName(std::string name);

private:
    void showMainMenu();
    int getMainCommand();
    int loadIntoRegister(std::string variableName);
    int executeCommand(int command);
    int executeLoadCommand();
    int executeStoreCommand();
    int executeAddCommand();
    int executePrintCommand();
    int getRegisterValue(int registerNumber);
    void displayRegisterName(int registerNumber);

private:
    std::string computerName_;
    int RegisterA_;
    int RegisterB_;
    int RegisterC_;
    int RegisterD_;
};


