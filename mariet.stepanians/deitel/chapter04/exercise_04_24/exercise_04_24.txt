a.
    // x = 5, y = 8
    if (8 == y) {
        if (5 == x) {
            std::cout << "@@@@@" << std::endl;
        } else {
            std::cout << "#####" << std::endl;
        }
        std::cout << "$$$$$" << std::endl;
        std::cout << "&&&&&" << std::endl;
    }

b. 
    // x = 5, y = 8
    if (8 == y) {
        if (5 == x) {
            std::cout << "@@@@@" << std::endl;
        }
    } else {
        std::cout << "#####" << std::endl;
        std::cout << "$$$$$" << std::endl;
        std::cout << "&&&&&" << std::endl;
    }

c. 
    // x = 5, y = 8
    if (8 == y) {
        if (5 == x) {
            std::cout << "@@@@@" << std::endl;
        } else {
            std::cout << "#####" << std::endl;
            std::cout << "$$$$$" << std::endl;
        }
        std::cout << "&&&&&" << std::endl;
    }

d.
    // x = 5, y = 7
    if (8 == y) {
        if (5 == x) {
            std::cout << "@@@@@" << std::endl;
        }
    } else {
        std::cout << "#####" << std::endl;
        std::cout << "$$$$$" << std::endl;
        std::cout << "&&&&&" << std::endl;
    }
    
