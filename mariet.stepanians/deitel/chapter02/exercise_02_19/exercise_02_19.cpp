#include <iostream>

int
main()
{
    
    int number1;
    int number2;
    int number3;
    
    
    std::cout << "Input three different integers: ";
    std::cin  >> number1 >> number2 >> number3;
    
    int sum = number1 + number2 + number3;
    int product = number1 * number2 * number3;
    int average = sum / 3; 
    
    int max = number1;
    int min = number1;
    
    if(min > number2) {
        min = number2;
    }
    if(max < number2) {
        max = number2;
    }
    if(min > number3) {
        min = number3;
    } 
    if(max < number3) {
        max = number3;
    }
    
    
    std::cout << "The sum is " << sum << std::endl;
    std::cout << "The product is " << product << std::endl;
    std::cout << "The average is " << average << std::endl;
    std::cout << "The smallest number is " << min << std::endl;
    std::cout << "The largest number is " << max << std::endl;
    
    return 0;
}
