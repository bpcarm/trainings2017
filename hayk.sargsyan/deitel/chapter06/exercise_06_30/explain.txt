With n/2 checking we are not checking even 
numbers (except 2) because they are not simple.
For low numbers (for example 3, 4, 5) it is 
effective. But for large numbers (for example 
1390, 9900, 9999) it is non-effective. For that
numbers the most effective effective way of 
checking is sqrt(n).
