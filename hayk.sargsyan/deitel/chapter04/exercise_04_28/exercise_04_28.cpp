#include <iostream>

int
main()
{
    int counterY = 1;

    while (counterY <= 8) { 
        int counterX = 1;

        if (0 == counterY % 2) { 
            std::cout << " ";
        }

        while (counterX <= 8) {    
            std::cout << "* ";
            ++counterX;
        }
        
        std::cout << std::endl;
        
        counterX = 1;
        ++counterY;
    }

    return 0;
}
