#include <iostream>

int
main()
{
    std::cout << "Year:\t" << "Amount of deposit" << std::endl;

    for (int rate = 5; rate <= 10; ++rate) {
        std::cout << "Rate:\t" << rate << "%" << std::endl;
        
        float accumulation = 1.0 + rate / 100.0;
        float principal = 1000.0;

        for (int year = 1; year <= 10; ++year) {
            principal *= accumulation;

            std::cout << year << ":\t" << principal << std::endl;
        }

        std::cout << std::endl;
    }

    return 0;
}

