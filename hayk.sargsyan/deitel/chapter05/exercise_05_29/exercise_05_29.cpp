#include <iostream>
#include <iomanip>

int
main()
{
    for (int percent = 5; percent <= 10; ++percent) {
        double rate = percent / 100.0;
        double amountRate = 1.0 + rate;
        double amount = 24.0;
        
        for (int year = 1; year <= 2017 - 1626; ++year) {
            amount *= amountRate;
            
            std::cout << std::fixed << "rate: " << rate
                      << std::setw(20) << "year: " << year
                      << std::setw(20) << "amount: " << amount << std::endl;
        }

        std::cout << std::endl;
    }

    return 0;
}
