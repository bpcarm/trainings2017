#include <iostream>

int
main()
{
    int factorial = 1;
    
    for (int counter = 1; counter <= 5; ++counter) {       
        factorial *= counter;
        
        std::cout << counter << " - " << factorial << std::endl;
    }

    return 0;
}
