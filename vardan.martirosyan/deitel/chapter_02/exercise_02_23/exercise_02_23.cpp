#include <iostream>

int
main()
{
    int number1, number2, number3, number4, number5;

    std::cout << "Enter five numbers: ";
    std::cin  >>  number1 >> number2 >> number3 >> number4 >> number5;

    int min = number1;
    if (min > number2) {
        min = number2;
    }
    if (min > number3) {
        min = number3;
    }
    if (min > number4) {
        min = number4;
    }
    if (min > number5) {
        min = number5;
    }
    int max = number1;
    if (max < number2) {
        max = number2;
    }
    if (max < number3) {
        max = number3;
    }
    if (max < number4) {
        max = number4;
    }
    if (max < number5) {
        max = number5;
    }
    std::cout << "The minimum value is " << min << std::endl;
    std::cout << "The maximum value is " << max << std::endl;

    return 0;
}

