#include <iostream>
#include <cassert>

void
square(const int side)
{
    assert(side > 0);
    for (int i = 1; i <= side; ++i) {
        for (int j = 1; j <= side; ++j) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }
}

int 
main()
{
    int number;
    std::cout << "Enter number: ";
    std::cin  >> number;

    if(number <= 0) {
        std::cerr << "Error 1: number can not be negative. " << std::endl;
        return 1;
    }
    square(number);
    return 0;
}
