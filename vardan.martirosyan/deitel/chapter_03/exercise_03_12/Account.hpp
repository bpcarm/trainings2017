class Account
{
public:
    Account(int accountBalance);
    void setBalance(int balance);
    int getBalance(int amount);
    int credit(int moneyAdd);
    int debit(int moneyWithdraw);
private:
    int accountBalance_;
};

