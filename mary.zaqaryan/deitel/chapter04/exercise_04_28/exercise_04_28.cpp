#include <iostream>

int 
main()
{  
    int counter1 = 1;
    
    while (counter1 <= 8 ) {
        if (0 == counter1 % 2) {
            std::cout << " ";
        }

        int counter2 = 1;
        while (counter2 <= 8) {
            std::cout << "* ";
            ++counter2;
        }
        std::cout << std::endl;
        ++counter1;
    }
    return 0; 
}
