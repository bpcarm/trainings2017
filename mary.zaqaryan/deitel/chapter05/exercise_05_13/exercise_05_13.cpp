#include <iostream>

int
main()
{
    for (int i = 1; i <= 5; ++i) {
        int number;
        std::cin >> number;

        if (number < 1 || number > 30) {
            std::cerr << "Error 1: Input number from 1 to 30.";
            return 1;
        }

        for (int j = 1; j <= number; ++j) {
            std::cout << "*";
        }

        std::cout << std::endl;
    }
}
