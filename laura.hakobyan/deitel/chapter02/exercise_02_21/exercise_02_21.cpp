///Print figures
#include <iostream> /// allows program to perform input and output

int 
main()
{
    std::cout << "Print figures" << "\n";
    std::cout << "*********" << "\t   ***   " << "\t  *  " << "\t    *    \n";
    std::cout << "*       *" << "\t *     * " << "\t *** " << "\t   * *   \n";
    std::cout << "*       *" << "\t*       *" << "\t*****" << "\t  *   *  \n";
    std::cout << "*       *" << "\t*       *" << "\t  *  " << "\t *     * \n";
    std::cout << "*       *" << "\t*       *" << "\t  *  " << "\t*       *\n";
    std::cout << "*       *" << "\t*       *" << "\t  *  " << "\t *     * \n";
    std::cout << "*       *" << "\t*       *" << "\t  *  " << "\t  *   *  \n";
    std::cout << "*       *" << "\t *     * " << "\t  *  " << "\t   * *   \n";
    std::cout << "*********" << "\t   ***   " << "\t  *  " << "\t    *    \n";

    return 0;
}
