#include <iostream> 
#include <iomanip> 

int 
main() 
{
    for (double rate = 0.05; rate <= 0.1; rate += 0.01) {
        std::cout << "Rate is " << rate * 100 << " %:" << std::endl; 
        std::cout << "Year" << std::setw(21) << "Amount on deposit" << std::endl;
        std::cout << std::fixed << std::setprecision(2);
        double principal = 1000.0; 
        double percent = 1.0 + rate;
        for (int year = 1; year <= 10; year++) {
            principal = principal * percent;
            std::cout << std::setw(4) << year << std::setw(21) << principal << std::endl; 
        }
    }
    return 0;
}

