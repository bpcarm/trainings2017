#include <iostream>
#include <climits>

int
main()
{
    int counter = 1;
    int largest = INT_MIN;
    int secondLargest = INT_MIN;

    while (counter != 11) {
        int number;
        std::cout << "Enter a number " << counter << ": ";
        std::cin >> number;
        ++counter;
        if (number > largest) {
            secondLargest = largest;
            largest = number;
        } else if (number > secondLargest) {
            secondLargest = number;
        }
    }

    std::cout << "The largest number is " << largest << " and second largest is " << secondLargest << std::endl;
	
    return 0;
}
		  
