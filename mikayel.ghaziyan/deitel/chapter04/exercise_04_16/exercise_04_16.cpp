#include <iostream>
#include <iomanip>

int
main()
{
    while (true) {
        int hours = 0;
        std::cout << "Enter hours worked (-1 to end): ";
        std::cin >> hours;
        if (-1 == hours){
            return 0;
        }
        if (hours < -1) {
            std::cerr << "Error 1: hours cannot have a negative value." << std::endl;
            return 1;
        }

        double rate = 0.00;
        std::cout << "Enter hourly rate of the worker ($00.00): ";
        std::cin >> rate >> std::setprecision(2) >> std::fixed;
    	if (rate < 0) {
            std::cerr << "Error 2: hourly rate cannot have a negative value.";
            return 2;
        }

        /// Checking extra hourse
        int exceed = 0;
        if (hours > 40) {
            exceed = hours - 40;
        }
 
        double extraRate = hours * 1.5;
        double salaryTotal = hours * rate + exceed * extraRate;
        std::cout << "Salary is: $" << salaryTotal << std::setprecision(2) << std::fixed << std::endl;
    } 
    return 0;
}
		  
