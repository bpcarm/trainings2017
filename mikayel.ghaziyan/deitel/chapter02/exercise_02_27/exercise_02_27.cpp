/// Mikayel Ghaziyan
/// 16/10/2017
/// Exercise 2.27

#include <iostream>

/// Beginning of the main
int
main()
{	
    std::cout << static_cast<int>('A') << " "
              << static_cast<int>('B') << " "
              << static_cast<int>('C') << " "
              << static_cast<int>('a') << " "
              << static_cast<int>('b') << " "
              << static_cast<int>('c') << " "
              << static_cast<int>('0') << " "
              << static_cast<int>('1') << " "
              << static_cast<int>('2') << " "
              << static_cast<int>('$') << " "
              << static_cast<int>('*') << " "
              << static_cast<int>('+') << " "
              << static_cast<int>('/') << " "
              << static_cast<int>(' ') << std::endl;

    return 0;
} /// End of the main

/// End of the file

