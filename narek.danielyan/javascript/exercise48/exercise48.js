$(document).ready(function () {
    "use strict";
    function toCamelCase(string) {
        var arr = string.split("-"), i = 0;
        for (i = 1; i < arr.length; ++i) {
            arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
        }       
        return arr.join("");
    }
    console.log(toCamelCase('background-color'));
});
