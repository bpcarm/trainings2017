$(document).ready(function () {
    "use strict";
    var someNumbers = [1, 12, 10, 15, 2];
    function sum(arr) {
        var total = arr.reduce(function (sum, current) {
                return sum + current;
            }, 0);
        return total;
    }
    console.log(sum(someNumbers));
});
