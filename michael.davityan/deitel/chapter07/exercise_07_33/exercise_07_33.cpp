#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <cstdlib>

int 
recursiveLinearSearch(const int integersArray[], const size_t sizeOfArray, const int searchKey, 
                      const size_t firstIdx = 0)
{
    if (integersArray[firstIdx] == searchKey) {
        return firstIdx;
    }
    if (firstIdx == sizeOfArray) {
        return -1;
    }
    return recursiveLinearSearch(integersArray, sizeOfArray, searchKey, firstIdx + 1);
} 

int
main()
{
    const size_t ARRAY_SIZE = 100;
    int integersArray[ARRAY_SIZE]; 
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert array elements\n";
    }
    for (size_t index = 0; index < ARRAY_SIZE; ++index) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "element "<< index + 1 << ": ";
        }
        std::cin >> integersArray[index];
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter integer search key: ";
    }
    int searchKey; 
    std::cin >> searchKey;
    const int elementIdx = recursiveLinearSearch(integersArray, ARRAY_SIZE, searchKey);
    if (elementIdx != -1) {
        std::cout << "Found value in element " << elementIdx + 1 << "\n";
    } else {
        std::cout << "Value not found" << std::endl;
    }
    return 0; 
} 

