Question....
For each of the following, write C++ statements that perform the specified task. 
------------------------------------------------------------------------------------------------------
Assume that unsigned integers are stored in two bytes and that the starting adress of the array is 
at location 1002500 in memory.
------------------------------------------------------------------------------------------------------
a. Declare an array of type unsigned int called values with five elements, and initialize the elements to the even integers from 2 to 10. Assume that the symbolic constant SIZE has been define das 5.
b. Declare a pointer vPtr that points to an object of type unsigned int.
c. Use a for statement to print the elements of array values using array subscript notation.
d. Write two separate statements that assign the starting address of array values to pointer variable 
vPtr.
e. Use a for statement to print the elements of array values using pointe r/offset notation.
f. Use a for statement to print the elements of array values using pointe r/offset notation with the 
array name as the pointer.
g. Use a for statement to print the elements of array values by subscripting the pointer to the array.
h. Refer to the fifth element of values using array subscript notation, pointer/offset notation with 
the array name as the pointer, pointer subscript notation and pointer/offset notation.
i. What address is referenced by vPtr + 3? What value is stored at that location?
j. Assuming that vPtr points to values[ 4 ], what address is referenced by vPtr -= 4? What value is 
stored at that location?

Answer.....
a. const size_t SIZE = 5; 
   size_t values[SIZE] = {2, 4, 6, 8, 10};

b. size_t *vPtr;

c.for (int i = 0; i < SIZE; ++i) {
      std::cout << values[i] << " ";
  }
  std::cout << std::endl;

d. vPtr = values;
   vPtr = &values[0];

e.for (int i = 0; i < SIZE; ++i) {
      std::cout << *(vPtr + i) << " ";
  }
  std::cout << std::endl;

f.for (int i = 0; i < SIZE; ++i) {
      std::cout << *(values + i) << " ";
  }
  std::cout << std::endl;

g.for (int i = 0; i < SIZE; ++i) {
      std::cout << vPtr[i] << " ";
  }
  std::cout << std::endl;

h. values[4], *(values + 4), vPtr[4], *(vPtr + 4).

i.Its referenced to adress 1002506, and the value of element by that adress is 8.

j.I think 1002500, and the element value by that adress is 2.

