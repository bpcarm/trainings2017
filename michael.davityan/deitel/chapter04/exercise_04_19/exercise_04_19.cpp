////
#include <iostream>
#include <climits>
#include <unistd.h>

int
main()
{
    int count = 1;
    int firstLargest = INT_MIN;
    int secondLargest = INT_MIN;
    while (count <= 10) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Insert integer" << count << ": ";
        }
        int number;
        std::cin >> number;
        if (firstLargest < number) {
            secondLargest = firstLargest;
            firstLargest = number;
        } else if (secondLargest < number) {
            secondLargest = number;
        } 
        ++count;
    }
    std::cout << "The most two largest numbers is " << secondLargest 
              << ", " << firstLargest << std::endl;

    return 0;
}
