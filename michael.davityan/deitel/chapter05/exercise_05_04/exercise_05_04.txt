Question: 
Find the error(s) in each of the following:
a. For ( x = 100, x >= 1, x++ )
    cout << x << endl;

b. The following code should print whether integer value is odd or even:
    switch ( value % 2 )
{
    case 0:
        cout << "Even integer" << endl;
    case 1:
        cout << "Odd integer" << endl;
}

c. The following code should output the odd integers from 19 to 1:
for ( x = 19; x >= 1; x += 2 )
cout << x << endl;

d. The following code should output the even integers from 2 to 100:
counter = 2;
do
{
    cout << counter << endl;
    counter += 2;
} While ( counter < 100 );

Answer: 

a.for (int x = 100; x >= 1; x--) /// (1)For- f -is upper case, (2)each condition must separate by ;,(3) ++_This one 
must be --,(4) I think x variable must be declared.

cout << x << endl;

b.
switch ( value % 2 ) /// after each of cases absent break;
{
    case 0:
    cout << "Even integer" << endl;
    break;

    case 1:
    cout << "Odd integer" << endl;
    break;
}

c. 
for ( x = 19; x >= 1; x -= 2 ) //// +=_This one must be -=.
cout << x << endl;

d.
counter = 2;
do
{
    cout << counter << endl;
    counter += 2;
} while ( counter <= 100 ); /// (a)for correct output on display of all even numbers from 2 to 100 <_this one must be <=, to include 100 at the end of cycle.... (b)While W-is upper case.

