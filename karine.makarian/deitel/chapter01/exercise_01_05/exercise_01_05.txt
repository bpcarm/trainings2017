Using machine independent languages we can easily  write programs; and they are also easy to understand, but programs written in  machine dependent languages work faster.  
