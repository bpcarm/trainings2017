#include <iostream>

int
main()
{
    for (int number = 1; number <= 256; number++) {    
        int index = 1;
        int counter = number;
        int binary = 0;
    
        while (counter != 0) {
            int binaryCharacter = counter % 2;
            counter /= 2;
            binary += binaryCharacter * index;
            index *= 10;
        }

        std::cout << "decimal: " << std::dec << number << "\t"
            << " | binary: " << binary << "\t"
            << " | octal: " << std::oct << number << "\t" 
            << " | hexadecimal: " << std::hex << number << "\t"
            << std::endl;
    }
     
    return 0;
}

