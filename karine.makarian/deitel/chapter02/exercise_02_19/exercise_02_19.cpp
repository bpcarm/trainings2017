#include <iostream>

int 
main()
{
    int number1;
    int number2;
    int number3;

    std::cout << "Input three different integers: ";
    std::cin >> number1 >> number2 >> number3; 

    std::cout << "Sum is " << (number1 + number2 + number3) << std::endl;
    std::cout << "Average is " << (number1 + number2 + number3)/3 << std::endl;
    std::cout << "Product is  " << (number1 * number2 * number3) << std::endl;

    int largest = number1;
    int smallest = number1;

    if (largest < number2) {
        largest = number2;        
    }   

    if (largest < number3) {
        largest = number3;
    }

    if (smallest > number2) {
        smallest = number2;
    }

    if (smallest > number3) {
        smallest = number3;
    }

    std::cout << "Smallest is " << smallest << std::endl;   
    std::cout << "Largest is " << largest << std::endl;
    return 0;
}

