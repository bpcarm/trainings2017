#include<iostream>
    
int
main()
{
    int number;

    std::cout << "Please enter a binary number: ";
    std::cin >> number;
   
    int decimalNumber = 0;
    int counter = 1;

    while (number != 0) {
        int digit = number % 10;
    
        if(digit != 0) {
            if(digit != 1) {
                std::cerr << "Error 1: Not a binary number." << std::endl;
                return 1;
            }
        } 
    
        decimalNumber += digit * counter;
        number /= 10;
        counter *= 2;
    }

    std::cout << "Decimal number is " << decimalNumber << std::endl;
    return 0;
}
