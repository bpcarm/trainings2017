#include <iostream>
#include <cmath>

int 
calculate(const double x) 
{
    const int y = static_cast<int>(std::floor(x + 0.5));

    return y;
}

int
main()
{
    double firstNumber;
    double secondNumber;
    double thirdNumber;

    std::cout << "Enter 3 numbers: ";
    std::cin >> firstNumber >> secondNumber >> thirdNumber;
    std::cout << std::endl;

    const int roundedNumber1 = calculate(firstNumber);
    const int roundedNumber2 = calculate(secondNumber);
    const int roundedNumber3 = calculate(thirdNumber);

    std::cout << "Original number" << "\t" << "Rounded number" << std::endl;
    std::cout << firstNumber << "\t\t" << roundedNumber1 << std::endl;
    std::cout << secondNumber << "\t\t" << roundedNumber2 << std::endl;
    std::cout << thirdNumber << "\t\t" << roundedNumber3 << std::endl;

    return 0;
}

