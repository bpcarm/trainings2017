#include <iostream>
#include <cmath>

int 
main()
{
    const int limit = 3;
    for (int count = 1; count <= limit; ++count) {
        double ceilArgument;
        std::cin >> ceilArgument;
        std::cout << "ceil(" << ceilArgument << ")=" << std::ceil(ceilArgument) << std::endl;
    }

    for (int count = 1; count <= limit; ++count) {
        double fabsArgument;
        std::cin >> fabsArgument;
        std::cout << "fabs(" << fabsArgument << ")=" << std::fabs(fabsArgument) << std::endl;
    }
    for (int count = 1; count <= limit; ++count) {
        double floorArgument;
        std::cin >> floorArgument;
        std::cout << "floor(" << floorArgument << ")=" << std::floor(floorArgument) << std::endl;
    }
    for (int count = 1; count <= limit; ++count) {
        double sqrtArgument;
        std::cin >> sqrtArgument;
        std::cout << "sqrt(" << sqrtArgument << ")=" << std::sqrt(sqrtArgument) << std::endl;
    }
    for (int count = 1; count <= limit; ++count) {
        double powBase;
        double powExponent;
        std::cin >> powBase >> powExponent;
        std::cout << "pow(" << powBase << ", " << powExponent << ")=" 
            << std::pow(powBase, powExponent) << std::endl;
    }
    for (int count = 1; count <= limit; ++count) {
        double expArgument;
        std::cin >> expArgument;
        std::cout << "exp(" << expArgument << ")=" << std::exp(expArgument) << std::endl;
    }
    for (int count = 1; count <= limit; ++count) {
        double logArgument;
        std::cin >> logArgument;
        std::cout << "log(" << logArgument << ")=" << std::log(logArgument) << std::endl;
    }
     for (int count = 1; count <= limit; ++count) {

         double log10Argument;
         std::cin >> log10Argument;
         std::cout << "log10(" << log10Argument << ")=" << std::log10(log10Argument) << std::endl;
     }
     for (int count = 1; count <= limit; ++count) {
         double fmodArgument1;
         double fmodArgument2;
         std::cin >> fmodArgument1 >> fmodArgument2;
         std::cout << "fmod(" << fmodArgument1 << ", " << fmodArgument2 << ")=" 
             << std::fmod(fmodArgument1, fmodArgument2) << std::endl;
     }
     for (int count = 1; count <= limit; ++count) {
         double sinArgument;
         std::cin >> sinArgument;
         std::cout << "sin(" << sinArgument << ")=" << std::sin(sinArgument) << std::endl;
     }
     for (int count = 1; count <= limit; ++count) {
         double cosArgument;
         std::cin >> cosArgument;
         std::cout << "cos(" << cosArgument << ")=" << std::cos(cosArgument) << std::endl;
     }
     for (int count = 1; count <= limit; ++count) {
         double tanArgument;
         std::cin >> tanArgument;
         std::cout << "tan(" << tanArgument << ")=" << std::tan(tanArgument) << std::endl;
     }

     return 0;
}

