Fill in the blank s in each of the following:
a. The names of the four elements of array p ( int p[ 4 ] ;) are p[0], p[1], p[2], p[3].
b. Naming an array, stating its type and specifying the number of elements in the array is called declaration of  the array.
c. By convention, the first subscript in a two-dimensional array identifies an element's rows and the second subscript identifies an
element's columns.
d. An m-by-n array contains m rows, n columns and m*n elements.
e. The name of the element in row 3 and column 5 of array d is d[3][5].
