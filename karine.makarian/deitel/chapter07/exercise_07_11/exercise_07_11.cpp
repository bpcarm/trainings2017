#include <iostream>

void 
bubbleSort(int arr[], const int size)
{
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size - 1; ++j) {
            if (arr[j] > arr [j + 1]) {
                int temp = arr[j + 1];
                arr[j + 1] = arr[j];
                arr[j] = temp;
            }
        }
    }
}

int
main()
{
    const int ARRAY_SIZE = 10;
    int arrayToSort[ARRAY_SIZE] = {5, 7, 15, 27, 89, 10, 6, 9, 1, 7};
    bubbleSort(arrayToSort, ARRAY_SIZE);
    for (int i = 0; i < ARRAY_SIZE; ++i) {
        std::cout << arrayToSort[i] << " ";
    }
    std::cout << std::endl;
    return 0;
}

